# GitLab Selenium Proxy/Server

This project is a work in progress (WIP).

## General Usage

Say you had your own Selenium server running on port `4444`
(see the "Start your own Selenium sever" below), you can point the GitLab Selenium
server proxy at that local server with the `GITLAB_TARGET_SELENIUM_REMOTE_URL` URL.

```sh
# macOS/Linux
GITLAB_TARGET_SELENIUM_REMOTE_URL=http://localhost:4444/wd/hub npm start

# Windows
set GITLAB_TARGET_SELENIUM_REMOTE_URL="http://localhost:4444/wd/hub"&&npm start
```

Now you point your Selenium tests at the GitLab Selenium server, `http://localhost:4545/wd/hub`


## GitLab CI Usage

Point your Selenium tests directly at `http://gitlab-selenium-server:4545/wd/hub`
after including the `gitlab/selenium-server` service.

`.gitlab-ci.yml`
```yml
test:
  stage: test
  script:
    - npm test
    # Show the logs for the GitLab Selenium Server service
    - curl http://gitlab-selenium-server:4545/logs || true
  artifacts:
    when: always
    paths:
      - selenium/
  variables:
    SELENIUM_REMOTE_URL: http://gitlab-selenium-server:4545/wd/hub
  services:
    - name: registry.gitlab.com/gitlab-org/gitlab-selenium-server:latest
      alias: gitlab-selenium-server
  tags:
    - docker
    - shared
```

### Use your own Selenium server

You can point the proxy at your own Selenium server with the `GITLAB_TARGET_SELENIUM_REMOTE_URL`
environment variable.

`.gitlab-ci.yml`
```yml
test:
  # ...
  variables:
    SELENIUM_REMOTE_URL: http://gitlab__selenium-server:4444/wd/hub
    GITLAB_TARGET_SELENIUM_REMOTE_URL: http://selenium__standalone-chrome:4444/wd/hub
  # ...
```


# Development

```sh
# macOS/Linux
GITLAB_TARGET_SELENIUM_REMOTE_URL=http://localhost:4444/wd/hub npm run start-dev

# Windows
set GITLAB_TARGET_SELENIUM_REMOTE_URL="http://localhost:4444/wd/hub"&&npm run start-dev
```


### Setting up the GitLab Docker Registry

See https://gitlab.com/snippets/73109#setup-the-container-registry


#### Setup a runner that can run Docker in Docker `dind` builds to push to the registry

Setup a Docker runner, add the privileged mode https://docs.gitlab.com/runner/executors/docker.html#the-privileged-mode
and restart `gitlab-ci-multi-runner restart`


# FAQ

### Use a different Selenium server/hub

Define the `SELENIUM_REMOTE_URL` environment variable. For GitLab CI, we define it in `.gitlab-ci.yml`

ex.
```sh
SELENIUM_REMOTE_URL=http://username:access_token@ondemand.saucelabs.com:80/wd/hub npm test
```

### Start your own Selenium sever

 1. Download your driver of choice
    - For example Chrome, https://sites.google.com/a/chromium.org/chromedriver/downloads and put it in `/usr/local/bin` to get it on your `PATH`
 1. Download Selenium server, http://docs.seleniumhq.org/download/
 1. Start the server
    ```sh
    java -Dwebdriver.chrome.driver=$(which chromedriver) -jar /Users/eric/Downloads/selenium-server-standalone-3.4.0.jar -port 4444
    ```
 1. Access the server with `http://localhost:4545/wd/hub`


# Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)
