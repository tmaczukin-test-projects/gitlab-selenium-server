const { isSessionReq, screenshotTriggers } =  require('./identify-command-from-req');

/**
 * Retuns true if any request contains a command that triggers a screenshot
 * @param {*} req 
 */
function doesRequestTriggerScreenshot(req) {
  return isSessionReq(req) && Object.keys(screenshotTriggers).some((trigger) => screenshotTriggers[trigger](req).result);
}

module.exports = doesRequestTriggerScreenshot;
