const Promise = require('bluebird');
const path = require('path');
const url = require('url');
const request = Promise.promisify(require('request'));

const { log } = require('./log');

function proxyRequest(target, req) {
  const parsedReqUrl = url.parse(req.url);
  const parsedTargetUrl = url.parse(target);

  const resultantTargetUrl = url.format(Object.assign({}, parsedReqUrl, {
    protocol: parsedTargetUrl.protocol,
    slashes: parsedTargetUrl.slashses,
    auth: parsedTargetUrl.auth,
    host: parsedTargetUrl.host,
    port: parsedTargetUrl.port,
    hostname: parsedTargetUrl.hostname,
    pathname: path.join('/', parsedTargetUrl.pathname, req.path),
  }));

  // We need to do some massaging back to a Buffer or string when we send it off again
  let body = undefined;
  if(Buffer.isBuffer(req.body)) {
    body = req.body;
  }
  else if(body) {
    body = JSON.stringify(req.body)
  }

  const originalFullUrl = `${req.protocol}://${req.get('host')}${req.originalUrl}`;
  log(`Proxying ${req.method} ${originalFullUrl} -> ${resultantTargetUrl}, is buffer? ${Buffer.isBuffer(req.body)}\n${body}`);

  return request({
    method: req.method,
    uri: resultantTargetUrl,
    headers: req.headers,
    body: body
  });
}

module.exports = proxyRequest;
