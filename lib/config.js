const path = require('path');

class Config {
  constructor() {
    this.data = {
      logDir: process.env['GITLAB_SELENIUM_PROXY_LOG_DIR'] ? path.resolve(__dirname, '../', process.env['GITLAB_SELENIUM_PROXY_LOG_DIR']) : path.resolve(__dirname, '../logs'),
      targetEndpoint: process.env['GITLAB_TARGET_SELENIUM_REMOTE_URL'] || 'http://localhost:4444/wd/hub'
    };
  }

  set(key, value) {
    this.data[key] = value;
  }

  get(key) {
    return this.data[key];
  }
}

module.exports = new Config();
