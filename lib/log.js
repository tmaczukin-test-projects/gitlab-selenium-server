const Promise = require('bluebird');
const path = require('path');
const fs = require('fs-extra');
const readFile = Promise.promisify(fs.readFile);
const outputFile = Promise.promisify(fs.outputFile);
const appendFile = Promise.promisify(fs.appendFile);

const config = require('./config');
const logPath = path.join(config.get('logDir'), `./log-${Date.now()}.txt`);
const logFileCreatedPromise = outputFile(logPath, '');

function toString(obj) {
  if(typeof obj === 'object') {
    return JSON.stringify(obj, null, 2);
  }

  return obj;
}

function log(...args) {
  // eslint-disable-next-line no-console
  console.log.apply(console, args);

  return logFileCreatedPromise
    .then(() => {
      appendFile(logPath, `[${new Date(Date.now()).toISOString()}] ${args.map(toString).join(' ')}\n`);
    });
}

function getLogs() {
  return readFile(logPath, 'utf8');
}

module.exports = {
  log,
  getLogs
};
