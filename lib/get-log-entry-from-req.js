const  { isSessionReq, screenshotTriggers }  = require('./identify-command-from-req');

/**
 * Retuns an object containing log entries
 * @param {*} req 
 * @param {*} commandId 
 */
function getLogEntryFromReq(req, commandId) {
  let result;

  if (isSessionReq(req)) {
    result = {
      id: commandId,
      type: null,
      path: req.path
    };
  }

  Object.keys(screenshotTriggers).some((trigger) => {
    const triggerData = screenshotTriggers[trigger](req);

    if (triggerData.result) { 
      result.type = triggerData.type;
    }

    return triggerData.result;
  });
  
  return result;
}

module.exports = getLogEntryFromReq;
