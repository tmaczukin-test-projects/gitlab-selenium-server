const Promise = require('bluebird');
const path = require('path');
const url = require('url');
const request = Promise.promisify(require('request'));

const { log } = require('./log');

const logMap = {
  /* * /
  :sessionId: {
    browser: [],
    client: [],
    driver: [],
    performance: [],
    server: [],
    // GitLab proprietary log, used for CI views
    gitlab: []
  }
  /* */
};

function updateLog(sessionId, type, logValue) {
  logMap[sessionId] = (logMap[sessionId] || {});
  const resultantLog = (logMap[sessionId][type] || []).concat(logValue);
  logMap[sessionId][type] = resultantLog;
  //log('updateLog resultantLog', sessionId, resultantLog);
  return resultantLog;
}


function fetchLogTypes(target, sessionId) {
  const parsedTargetUrl = url.parse(target);

  const resultantTargetUrl = url.format(Object.assign({}, parsedTargetUrl, {
    pathname: path.join('/', parsedTargetUrl.pathname, `/session/${sessionId}/log/types`),
  }));

  return request({
    method: 'get',
    uri: resultantTargetUrl,
    json: true
  })
    .then((res) => {
      return res.body.value;
    });
}

function fetchLog(target, sessionId, type) {
  const parsedTargetUrl = url.parse(target);

  const resultantTargetUrl = url.format(Object.assign({}, parsedTargetUrl, {
    pathname: path.join('/', parsedTargetUrl.pathname, `/session/${sessionId}/log`),
  }));

  return request({
    method: 'post',
    uri: resultantTargetUrl,
    // For some reason the `json` option results in an error
    // `java.lang.ClassCastException` -> `java.lang.String cannot be cast to java.util.HashMap`
    //json: true,
    body: JSON.stringify({
      type
    })
  })
    .then((res) => {
      let log = [];
      try {
        const resData = JSON.parse(res.body);
        log = resData.value;
      } catch(err) {
        // swallow
      }

      return log;
    });
}

function fetchAllLogs(target, sessionId) {
  return fetchLogTypes(target, sessionId)
    .then((types) => {
      const logPromises = types.map((type) => {
        return fetchLog(target, sessionId, type)
          .then((log) => {
            return updateLog(sessionId, type, log);
          });
      });

      return Promise.all(logPromises)
        .then(() => logMap[sessionId]);
    });
}

module.exports = {
  updateLog,
  fetchLogTypes,
  fetchLog,
  fetchAllLogs
};
