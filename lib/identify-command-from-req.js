
// TODO: Other trigger criteria
// see https://support.saucelabs.com/hc/en-us/articles/225265388-Selenium-Commands-that-Trigger-Screenshots

function isSessionReq(req) {
  return (/session\/[a-f\d-]+/i).test(req.path);
}

function isClicking(req) {
  return {
    type: 'click',
    result: (/element\/.+?\/click\/?$/).test(req.path),
  };
}

function isSendingKeys(req) {
  return {
    type: 'keys',
    result: (/element\/.+?\/value\/?$/).test(req.path),
  };
}

function isExecutingJavascript(req) {
  return {
    type: 'executing-javascript',
    result: (/session\/.+?\/execute\/?/).test(req.path),
  };
}

module.exports = {
  isSessionReq,
  screenshotTriggers: {
    isClicking,
    isSendingKeys,
    isExecutingJavascript,
  }
};
