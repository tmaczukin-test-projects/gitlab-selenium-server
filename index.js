const { log } = require('./lib/log');
const app = require('./server/app');

// Log out any error that may have been lost
process.on('unhandledRejection', function(reason/*, promise*/) {
  log('unhandledRejection', reason, reason.stack);
});

const port = 4545;
const server = app.listen(port, function () {
  log(`GitLab Selenium proxy listening on port ${port}!`);
});

process.on('SIGINT', function() {
  log('Received SIGINT, gracefully shutting down');
  server.close();
  process.exit();
});


module.exports = server;
